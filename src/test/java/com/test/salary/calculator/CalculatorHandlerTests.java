/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.salary.calculator;

import com.test.salary.RoutingV1;
import com.test.salary.Service;
import com.test.salary.TestServerConfig;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

//ofc this is just the beginning
public class CalculatorHandlerTests {

	@Autowired
	private AnnotationConfigApplicationContext context;

	private WebTestClient testClient;

	@Before
	public void createTestClient() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				TestServerConfig.class,
				CalculatorHandler.class,
				CalculateContractNetIncomeCommand.class,
				SalaryCalculatorRepository.class,
				RoutingV1.class
		);
		Service service = new Service(context);
		this.testClient = WebTestClient.bindToRouterFunction(service.getRoutingV1().create())
				.configureClient()
				.baseUrl("http://localhost")
				.build();
	}

	@Test
	public void returns_not_found_when_country_not_found() throws Exception {
		this.testClient.get()
				.uri("/v1/RU/contract/net/monthly?dailyIncome=100")
				.exchange()
				.expectStatus()
				.isNotFound();
	}

	@Test
	public void returns_unprocessable_entity_when_invalid_param() throws Exception {
		this.testClient.get()
				.uri("/v1/RU/contract/net/monthly?dailyIncome=aa")
				.exchange()
				.expectStatus()
				.isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@Test
	public void returns_de_contract_net_monthly_salary() throws Exception {
		this.testClient.get()
				.uri("/v1/DE/contract/net/monthly?dailyIncome=100")
				.exchange()
				.expectStatus().isOk()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBody();
	}
}