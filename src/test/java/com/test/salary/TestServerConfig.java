package com.test.salary;

import com.test.salary.calculator.country.SalaryCalculator;
import com.test.salary.calculator.country.context.CountryTaxSystemContext;
import com.test.salary.calculator.currencyexchange.CurrencyExchangeService;
import com.test.salary.calculator.currencyexchange.CurrencyValue;
import com.test.salary.calculator.employment.EmploymentContractFormula;
import com.test.salary.calculator.employment.StandardEmploymentContractFormula;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

@Configuration
public class TestServerConfig {

	@Bean
	public SalaryCalculator produceWageCalculatorService() {
		return new SalaryCalculator() {
			@Override
			public String getCountryCode() {
				return "DE";
			}

			@Override
			public Optional<EmploymentContractFormula> getEmploymentContractFormula(String type) {
				return Optional.of(new StandardEmploymentContractFormula(new CountryTaxSystemContext(
						Monetary.getCurrency("EUR"),
						new BigDecimal("1000"),
						new BigDecimal("10")
				)));
			}
		};
	}

	@Bean
	public CurrencyExchangeService produceCurrencyExchange() {
		return (currency, date) -> new CurrencyValue(
				"de",
				Collections.singletonList(new CurrencyValue.CurrencyRate(
						"2016-04-04",
						new BigDecimal("4.11")
				))
		);
	}

	@Bean(name = "output")
	public CurrencyUnit produceConfig() {
		return Monetary.getCurrency("PLN");
	}
}
