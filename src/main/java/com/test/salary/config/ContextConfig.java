package com.test.salary.config;

import com.test.salary.RoutingV1;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

@Configuration
@ComponentScan("com.test")
public class ContextConfig {

	@Bean
	public RoutingV1 produceRouting() {
		return new RoutingV1();
	}

	@Bean(name = "output")
	public CurrencyUnit produceConfig() {
		return Monetary.getCurrency("PLN");
	}
}
