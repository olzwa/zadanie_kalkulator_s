package com.test.salary.calculator;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DefaultResponse<T> {
	@JsonProperty
	private T result;

	public DefaultResponse(T result) {
		this.result = result;
	}

	public DefaultResponse() {
	}

	@Override
	public String toString() {
		return "DefaultResponse{" +
				"result=" + result +
				'}';
	}
}
