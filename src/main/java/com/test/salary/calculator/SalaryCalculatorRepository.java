package com.test.salary.calculator;

import com.test.salary.Service;
import com.test.salary.calculator.country.SalaryCalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class SalaryCalculatorRepository {
	private static final Logger logger = LogManager.getLogger(SalaryCalculatorRepository.class);

	private final Map<String, SalaryCalculator> calculators;

	public SalaryCalculatorRepository(List<SalaryCalculator> countryTaxSystemContexts) {
		this.calculators = countryTaxSystemContexts
				.stream()
				.collect(Collectors.toMap(SalaryCalculator::getCountryCode, Function.identity()));

		logger.info("registered [" + calculators.size() + "] calculators");
	}

	Optional<SalaryCalculator> get(String countryCode) {
		return Optional.ofNullable(calculators.get(countryCode));
	}
}
