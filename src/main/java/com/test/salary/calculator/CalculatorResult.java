package com.test.salary.calculator;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.money.MonetaryAmount;
import javax.money.format.MonetaryFormats;
import java.util.Objects;

public class CalculatorResult {
	@JsonProperty
	private String amount;

	@JsonProperty
	private String currencyCode;

	public CalculatorResult() {
	}

	private CalculatorResult(String amount, String currencyCode) {
		this.amount = amount;
		this.currencyCode = currencyCode;
	}

	static CalculatorResult from(MonetaryAmount monetaryAmount) {
		return new CalculatorResult(
				monetaryAmount.getNumber().toString(),
				monetaryAmount.getCurrency().getCurrencyCode()
		);
	}

	@Override
	public String toString() {
		return "CalculatorResult{" +
				"amount='" + amount + '\'' +
				", currencyCode='" + currencyCode + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		CalculatorResult that = (CalculatorResult) o;
		return Objects.equals(amount, that.amount) &&
				Objects.equals(currencyCode, that.currencyCode);
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, currencyCode);
	}
}