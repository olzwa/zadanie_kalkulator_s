package com.test.salary.calculator.currencyexchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class CurrencyValue {
	private final String code;
	private final List<CurrencyRate> rates;

	public CurrencyValue(@JsonProperty("code") String code,
											 @JsonProperty("rates") List<CurrencyRate> rates) {
		this.code = code;
		this.rates = rates;
	}

	public String getCode() {
		return code;
	}

	public CurrencyRate getRate() {
		return rates.get(0);
	}

	public static class CurrencyRate {
		private final String effectiveDate;
		private final BigDecimal mid;

		public CurrencyRate(@JsonProperty("effectiveDate") String effectiveDate,
												@JsonProperty("mid") BigDecimal mid) {
			this.effectiveDate = effectiveDate;
			this.mid = mid;
		}

		public String getEffectiveDate() {
			return effectiveDate;
		}

		public BigDecimal getMid() {
			return mid;
		}
	}
}


