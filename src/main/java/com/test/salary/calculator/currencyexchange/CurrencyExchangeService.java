package com.test.salary.calculator.currencyexchange;

import javax.money.CurrencyUnit;
import java.time.LocalDate;

public interface CurrencyExchangeService {

	CurrencyValue get(CurrencyUnit currency, LocalDate date);

}
