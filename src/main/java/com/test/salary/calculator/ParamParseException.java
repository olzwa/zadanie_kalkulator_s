package com.test.salary.calculator;

public class ParamParseException extends RuntimeException {
	public ParamParseException() {
	}

	public ParamParseException(Throwable cause) {
		super(cause);
	}
}
