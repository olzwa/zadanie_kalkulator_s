package com.test.salary.calculator;

import com.test.salary.calculator.country.SalaryCalculatorNotFoundException;
import com.test.salary.calculator.currencyexchange.CurrencyExchangeService;
import com.test.salary.calculator.currencyexchange.CurrencyValue;
import com.test.salary.calculator.employment.EmploymentContractTypeNotFoundException;
import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.inject.Named;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.TemporalUnit;

import static java.time.temporal.ChronoUnit.DAYS;

@Component
public class CalculateContractNetIncomeCommand {
	private final SalaryCalculatorRepository salaryCalculatorRepository;
	private final CurrencyExchangeService currencyExchangeService;
	private final CurrencyUnit outputCurrency;

	@Inject
	public CalculateContractNetIncomeCommand(CurrencyExchangeService currencyExchangeService,
																					 SalaryCalculatorRepository salaryCalculatorRepository,
																					 @Named("output") CurrencyUnit outputCurrency) {
		this.salaryCalculatorRepository = salaryCalculatorRepository;
		this.currencyExchangeService = currencyExchangeService;
		this.outputCurrency = outputCurrency;
	}

	MonetaryAmount execute(String countryCode, String contractType, BigDecimal grossDailyIncome) {
		BigDecimal grossIncomeInMonth = toMonth(grossDailyIncome);
		MonetaryAmount netIncome = calculateNetIncome(countryCode, contractType, grossIncomeInMonth);
		if (netIncome.getCurrency().equals(outputCurrency)) {
			return netIncome;
		} else {
			return toCurrency(netIncome, outputCurrency);
		}
	}

	private BigDecimal toMonth(BigDecimal amount) {
		return amount.multiply(BigDecimal.valueOf(22));//in requirements month always is fixed to 22 days
	}

	private MonetaryAmount calculateNetIncome(String countryCode, String contractType, BigDecimal grossDayIncome) {
		return salaryCalculatorRepository.get(countryCode)
				.orElseThrow(SalaryCalculatorNotFoundException::new)
				.getEmploymentContractFormula(contractType)
				.orElseThrow(EmploymentContractTypeNotFoundException::new)
				.apply(grossDayIncome);
	}

	private MonetaryAmount toCurrency(MonetaryAmount netIncome, CurrencyUnit currencyUnit) {
		CurrencyValue currencyValue = currencyExchangeService.get(
				Money.from(netIncome).getCurrency(),
				LocalDate.now().minus(1, DAYS) // currency exchange rate from yesterday
		);
		return Monetary.getDefaultAmountFactory()
				.setAmount(netIncome
						.multiply(currencyValue.getRate().getMid())
						.with(Monetary.getDefaultRounding()))
				.setCurrency(currencyUnit)
				.create();
	}
}

