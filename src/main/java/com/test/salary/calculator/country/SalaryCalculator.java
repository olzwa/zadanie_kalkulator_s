package com.test.salary.calculator.country;

import com.test.salary.calculator.employment.EmploymentContractFormula;

import java.util.Optional;

public interface SalaryCalculator {

	String getCountryCode();

	Optional<EmploymentContractFormula> getEmploymentContractFormula(String type);
}
