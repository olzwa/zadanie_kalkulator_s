package com.test.salary.calculator.country.context;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.math.BigDecimal;

public class CountryTaxSystemContext {
	private final CurrencyUnit currencyUnit;
	private final Money labourCostTotal;
	private final BigDecimal incomeTaxPercent;

	public CountryTaxSystemContext(CurrencyUnit currencyUnit, BigDecimal labourCostTotal, BigDecimal incomeTaxPercent) {
		this.currencyUnit = currencyUnit;
		this.labourCostTotal = Money.of(labourCostTotal, currencyUnit);
		this.incomeTaxPercent = incomeTaxPercent;
	}

	public CurrencyUnit getCurrencyUnit() {
		return currencyUnit;
	}

	public Money getLabourCostTotal() {
		return labourCostTotal;
	}

	public BigDecimal getIncomeTaxPercent() {
		return incomeTaxPercent;
	}

	public Money createMoney(BigDecimal amount) {
		return Money.of(amount, currencyUnit);
	}

	public static CountryTaxSystemContext from(CountryTaxSystemContextEntity contextEntity) {
		CurrencyUnit currency = Monetary.getCurrency(contextEntity.getCurrencyUnit());
		return new CountryTaxSystemContext(
				currency,
				contextEntity.getLabourCostTotal(),
				contextEntity.getIncomeTaxPercent()
		);
	}
}
