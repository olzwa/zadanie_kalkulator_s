package com.test.salary.calculator.country.context;

import java.math.BigDecimal;

public class CountryTaxSystemContextEntity {

	private String id;

	private String countryCode;

	private String currencyUnit;

	private BigDecimal labourCostTotal;

	private BigDecimal incomeTaxPercent;

	CountryTaxSystemContextEntity(String id, String countryCode, String currencyUnit,
																BigDecimal labourCostTotal, BigDecimal incomeTaxPercent) {
		this.id = id;
		this.countryCode = countryCode;
		this.currencyUnit = currencyUnit;
		this.labourCostTotal = labourCostTotal;
		this.incomeTaxPercent = incomeTaxPercent;
	}

	public String getId() {
		return id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	String getCurrencyUnit() {
		return currencyUnit;
	}

	BigDecimal getLabourCostTotal() {
		return labourCostTotal;
	}

	BigDecimal getIncomeTaxPercent() {
		return incomeTaxPercent;
	}
}