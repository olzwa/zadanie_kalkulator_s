package com.test.salary.calculator.country;

import com.test.salary.calculator.country.context.CountryTaxSystemContext;
import com.test.salary.calculator.country.context.CountryTaxSystemContextEntity;
import com.test.salary.calculator.country.context.CountryTaxSystemContextRepository;
import com.test.salary.calculator.employment.EmploymentContractFormula;
import com.test.salary.calculator.employment.StandardEmploymentContractFormula;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class GermanySalaryCalculator implements SalaryCalculator {
	public final static String COUNTRY_CODE = "DE";

	private CountryTaxSystemContext countryTaxSystemContext;

	private final Map<String, EmploymentContractFormula> employmentContractTypes = new HashMap<>();

	public GermanySalaryCalculator(CountryTaxSystemContextRepository repository) {
		CountryTaxSystemContextEntity contextEntity = repository.findByCountryCode(COUNTRY_CODE);
		countryTaxSystemContext = CountryTaxSystemContext.from(contextEntity);
		this.putEmploymentFormula(new StandardEmploymentContractFormula(countryTaxSystemContext));
	}

	@Override
	public String getCountryCode() {
		return COUNTRY_CODE;
	}

	@Override
	public Optional<EmploymentContractFormula> getEmploymentContractFormula(String type) {
		return Optional.ofNullable(employmentContractTypes.get(type.toUpperCase()));
	}

	private void putEmploymentFormula(EmploymentContractFormula employmentContractFormula) {
		employmentContractTypes.put(employmentContractFormula.getName(), employmentContractFormula);
	}
}
