package com.test.salary.calculator.country.context;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository //Could be MongoDB repository for example
public class CountryTaxSystemContextRepository {

	public CountryTaxSystemContextEntity findByCountryCode(String countryCode) {
		if (countryCode.toUpperCase().equals("UK")) {
			return new CountryTaxSystemContextEntity(
					"1",
					"UK",
					"GBP",
					new BigDecimal("600"),
					new BigDecimal("25")
			);
		}
		if (countryCode.toUpperCase().equals("DE")) {
			return new CountryTaxSystemContextEntity(
					"2",
					"DE",
					"EUR",
					new BigDecimal("800"),
					new BigDecimal("20")
			);
		}
		if (countryCode.toUpperCase().equals("PL")) {
			return new CountryTaxSystemContextEntity(
					"3",
					"PL",
					"PLN",
					new BigDecimal("1200"),
					new BigDecimal("19")
			);
		}
		return null;
	}
}
