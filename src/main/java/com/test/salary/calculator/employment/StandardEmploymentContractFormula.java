package com.test.salary.calculator.employment;

import com.test.salary.calculator.country.context.CountryTaxSystemContext;
import org.javamoney.moneta.function.MonetaryOperators;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Function;

public class StandardEmploymentContractFormula implements EmploymentContractFormula {
	private final String name = "CONTRACT";

	private final CountryTaxSystemContext countryTaxSystemContext;

	private Function<MonetaryAmount, MonetaryAmount> labourCost;

	private Function<MonetaryAmount, MonetaryAmount> incomeTax;

	private Map<BiPredicate<MonetaryAmount, CountryTaxSystemContext>, Function<MonetaryAmount, MonetaryAmount>> rules = new HashMap<>();


	public StandardEmploymentContractFormula(CountryTaxSystemContext countryTaxSystemContext) {
		this.countryTaxSystemContext = countryTaxSystemContext;
		creteTaxFunctions(countryTaxSystemContext);
	}

	private void creteTaxFunctions(CountryTaxSystemContext countryTaxSystemContext) {
		this.labourCost = income -> income.subtract(countryTaxSystemContext.getLabourCostTotal());

		this.incomeTax = income -> {
			if (income.getNumber().intValue() > 0) {
				return MonetaryOperators
						.percent(BigDecimal.valueOf(100).subtract(countryTaxSystemContext.getIncomeTaxPercent()))
						.apply(income);
			}
			return income;
		};
	}

	@Override
	public MonetaryAmount apply(BigDecimal income) {
		return labourCost
				.andThen(incomeTax)
				.apply(countryTaxSystemContext.createMoney(income));
	}

	@Override
	public String getName() {
		return name;
	}
}
