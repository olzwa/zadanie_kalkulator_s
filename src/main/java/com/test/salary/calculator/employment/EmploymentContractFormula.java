package com.test.salary.calculator.employment;


import javax.money.MonetaryAmount;
import java.math.BigDecimal;

public interface EmploymentContractFormula {
	String getName();

	MonetaryAmount apply(BigDecimal income);
}
