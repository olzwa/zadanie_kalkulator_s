package com.test.salary.calculator.employment;

import com.test.salary.calculator.CommandException;

public class EmploymentContractTypeNotFoundException extends CommandException {
}
