/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.salary.calculator;

import com.test.salary.calculator.country.SalaryCalculatorNotFoundException;
import com.test.salary.calculator.employment.EmploymentContractTypeNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import javax.money.MonetaryAmount;
import java.math.BigDecimal;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;

@Component
public class CalculatorHandler {
	private static final Logger logger = LogManager.getLogger(CalculatorHandler.class);

	private CalculateContractNetIncomeCommand command;

	@Inject
	public CalculatorHandler(CalculateContractNetIncomeCommand command) {
		this.command = command;
	}

	public Mono<ServerResponse> handle(ServerRequest request) {
		try {
			String countryCode = request.pathVariable("countryCode");
			BigDecimal dailyIncome = parseQueryParam(request, "dailyIncome");
			String contractEmploymentType = "contract";//in requirements there is only one contract type

			MonetaryAmount result = command.execute(countryCode, contractEmploymentType, dailyIncome);
			return ServerResponse
					.ok()
					.contentType(APPLICATION_JSON)
					.header("Access-Control-Allow-Origin", "*")//for demo purposes
					.body(fromObject(new DefaultResponse<>(CalculatorResult.from(result))));
		} catch (SalaryCalculatorNotFoundException | EmploymentContractTypeNotFoundException exception) {
			logger.warn(exception);
			return ServerResponse.notFound().build();
		} catch (ParamParseException exception) {
			logger.error(exception);
			return ServerResponse.unprocessableEntity().build();
		} catch (Exception exception) {
			logger.error(exception);
			return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	private BigDecimal parseQueryParam(ServerRequest request, String paramName) {
		try {
			return request
					.queryParam(paramName)
					.map(BigDecimal::new)
					.map(bigDecimal -> bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP))
					.orElseThrow(ParamParseException::new);
		} catch (Exception exception) {
			throw new ParamParseException(exception);
		}
	}

}
