package com.test.salary;

import com.test.salary.calculator.CalculatorHandler;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import javax.inject.Inject;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;

public class RoutingV1 {

	@Inject
	private CalculatorHandler calculatorHandler;

	public RouterFunction<ServerResponse> create() {
		return nest(
				accept(APPLICATION_JSON),
				RouterFunctions.route(
						GET("/v1/{countryCode:[A-Z]{2}}/contract/net/monthly"),
						calculatorHandler::handle
				)
		);
	}
}