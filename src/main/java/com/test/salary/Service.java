/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.test.salary;

import com.test.salary.config.ContextConfig;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter;
import org.springframework.http.server.reactive.ServletHttpHandlerAdapter;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.ipc.netty.http.server.HttpServer;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import static org.springframework.web.reactive.function.server.RouterFunctions.toHttpHandler;


public class Service {
	private static final Logger logger = LogManager.getLogger(Service.class);

	private RoutingV1 routingV1;

	public Service(AnnotationConfigApplicationContext context) {
		routingV1 = context.getBean(RoutingV1.class);
	}

	public static void main(String[] args) throws Exception {
		logger.info("reading property file " + args[0]);
		Properties properties = new Properties();
		try (FileReader in = new FileReader(args[0])) {
			properties.load(in);
			logger.info(properties);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Service service = new Service(new AnnotationConfigApplicationContext(
				ContextConfig.class
		));

		String host = properties.getProperty("service.host");
		int port = Integer.valueOf(properties.getProperty("service.port"));

		service.startReactorServer(host, port);
		//service.startTomcatServer();

		System.out.println("Press ENTER to exit.");
		System.in.read();
	}

	private void startReactorServer(String host, int port) throws InterruptedException {
		RouterFunction<ServerResponse> route = routingV1.create();
		HttpHandler httpHandler = toHttpHandler(route);

		ReactorHttpHandlerAdapter adapter = new ReactorHttpHandlerAdapter(httpHandler);
		HttpServer server = HttpServer.create(host, port);
		server.newHandler(adapter).block();
	}

	private void startTomcatServer(String host, int port) throws LifecycleException {
		RouterFunction<?> route = routingV1.create();
		HttpHandler httpHandler = toHttpHandler(route);

		Tomcat tomcatServer = new Tomcat();
		tomcatServer.setHostname(host);
		tomcatServer.setPort(port);
		Context rootContext = tomcatServer.addContext("", System.getProperty("java.io.tmpdir"));
		ServletHttpHandlerAdapter servlet = new ServletHttpHandlerAdapter(httpHandler);
		Tomcat.addServlet(rootContext, "httpHandlerServlet", servlet);
		rootContext.addServletMapping("/", "httpHandlerServlet");
		tomcatServer.start();
	}

	public RoutingV1 getRoutingV1() {
		return routingV1;
	}
}
